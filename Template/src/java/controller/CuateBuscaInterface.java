package controller;

import java.util.Collection;

/**
 * * @author ISA-CAMBER
 */
public interface CuateBuscaInterface {

    public Cuate BuscaCuate(String login, String pwd);

    public Collection<Cuate> ListaCuates();
}
