/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/** 
  * @author ISA-CAMBER
 */

@ManagedBean(name="buscaCuate", eager=true)
@ApplicationScoped
public class MapeaCuates implements CuateBuscaInterface{
    private final Map<String,Cuate> cuates;
    private final Collection <Cuate> lista=new ArrayList<>();
    public MapeaCuates() {
        cuates=new HashMap<>();
        
        addCuate(new Cuate ("cuate1", "cuate1", "Lopez Avila", "Juan", 2345.60,"avatar3"));
        addCuate(new Cuate ("cuate2", "123456", "Perez", "Ana", 35.00,"avatar2"));
        addCuate(new Cuate ("cuate3", "cuate3", "Ortega Viera", "Alma", 35.00,"avatar1"));
        addCuate(new Cuate ("cuate4","456789","Becerril Mira","Mary Carmen",45.00,"avatar5"));
    }
    
   @Override
    public Cuate BuscaCuate(String login, String pwd)
    {
        Cuate cuate = null;
        if (login != null)
        {
            cuate = cuates.get(login.toLowerCase());
            if (cuate != null)
            {
                if (!cuate.getPwd().equals(pwd))
                {
                    return null;
                }
            }
        }
        return cuate;
    }

    
    private void addCuate(Cuate cuate)
    {
        cuates.put(cuate.getUser(), cuate);
        lista.add(cuate);
    
    }

    @Override
    public Collection<Cuate> ListaCuates() {

        return lista;
    }
    
}
